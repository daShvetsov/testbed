#!/usr/bin/env bash

source /home/dshvetsov/.opam/opam-init/variables.sh

frenetic http-controller \
             --openflow-port 6652 > /tmp/frenetix.log 2>&1
