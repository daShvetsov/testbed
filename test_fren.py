#!/usr/bin/env python
import sys
import time
sys.path.insert(0, '/home/dshvetsov/mininet')


from mininet.topo import Topo, SingleSwitchTopo
from mininet.net import Mininet
from mininet.util import dumpNodeConnections
from mininet.node import Controller

class FRENETIC_OLD ( Controller ) :
    def __init__( self, name='frenetic', cdir='/home/dshvetsov/5course',
            command='execute', port=6652, cargs='./run_frenetic_old.sh %d', **kwargs):
        Controller.__init__(self, name, cdir=cdir, port=port, command=command,
                cargs=cargs, **kwargs)

class PYTHON_APP ( Controller ):
    def __init__( self, name='frenetic', cdir='/home/dshvetsov/frenetic/src/lang/python',
            command='execute', port=9999, cargs='./run_learning.py %d', **kwargs):
        Controller.__init__(self, name, cdir=cdir, port=port, command=command,
                cargs=cargs, **kwargs)

trigged = False
def smartSum(text, first, second):

    def helper(line):
        global trigged
        if trigged and line.count(second) > 0:
            trigged = False
            return 1
        if line.count(first) > 0 :
            trigged = True
        else :
            trigged = False
        return 0

    ret =  reduce(lambda x,y : x + helper(y), text.split('\n'), 0)
    trigged = False
    return ret

def simpleTest(n = 4):
    "Simple test"
    topo = SingleSwitchTopo(n)
    net = Mininet(topo,controller=lambda name : FRENETIC_OLD(name=name), autoSetMacs=True)
    c2 = net.addController('c2', lambda name: PYTHON_APP(name=name))
    time.sleep(0.5)
    net.start()
    time.sleep(3)
    switches = [net.get('s1')]#[net.get(i) for i in topo.sws]
    files = []
    for j in switches :
        filename = "snoop" + str( j.dpid ) + '.dmp'
        j.dpctl('snoop', '2>&1 >/dev/null | grep -E "OFPT_FLOW_MOD|OFPT_PACKET_IN|dl_type=0x88cc|OFPT_PACKET_OUT|OFPT_FLOW_REMOVED" > ' + filename + '&')
        files.append(filename)
    time.sleep(1)
#dumpNodeConnections(net.hosts)
    nowtime = time.time()
    loss = net.pingAll()
    h3 = net.get('h3')
    h3.cmd('iperf -c 10.0.0.4 5001 --udp -b 1000000000')
    nowtime = time.time() - nowtime
    if loss > 10 :
        print "WARNING: in %d hosts too many loss" %n
    net.stop()
    flowmods = 0
    packet_ins = 0
    lldp_packet_ins = 0
    packet_outs = 0
    lldp_packet_outs = 0
    flow_removeds = 0

    for j in files:
        f = open(j)
        text = f.readlines()
        text = ''.join(text)
        flowmods += text.count('OFPT_FLOW_MOD')
        packet_ins += text.count('OFPT_PACKET_IN')
        lldp_packet_ins += smartSum(text, "OFPT_PACKET_IN", "dl_type=0x88cc")
        packet_outs += text.count('OFPT_PACKET_OUT')
        lldp_packet_outs +=smartSum(text, "OFPT_PACKET_OUT", "dl_type=0x88cc")
        flow_removeds += text.count('OFPT_FLOW_REMOVED')

    f.close()
    f = open('many_tests.csv', 'a')
    f.write("%d, %d, %d, %d, %d, %d, %d, %f, %f\n" % (n , flowmods , packet_ins , lldp_packet_ins , packet_outs , lldp_packet_outs , flow_removeds , loss, nowtime))
    f.close()

    print """FlowMod: %d,
Packet_in: %d,
lldp_Packet: %d,
Packet_out: %d,
lldp_Packet_out: %d,
FlowRemoved: %d,
Lost: %f,
Time: %f""" % (flowmods, packet_ins, lldp_packet_ins, packet_outs, lldp_packet_outs, flow_removeds, loss, nowtime)

if __name__ == '__main__':
    for j in range(4):
        for i in [11, 16, 17]:
            simpleTest(i)
#topos = {'mytopo' : ( lambda n: BinTreeTopo(n) ) }
