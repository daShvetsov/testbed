#!/usr/bin/env bash


sudo mn --topo=single,10 \
        --controller=remote,port=6653 \
        --controller=remote,port=6652 \
        --switch=ovsk,protocols=OpenFlow13 \
        --mac
