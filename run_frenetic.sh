#!/usr/bin/env bash

source /home/dshvetsov/.opam/opam-init/variables.sh

frenetic openflow13 \
             --policy-file my-pol.kat \
             --openflow-port 6652 > /tmp/frenetix.log 2>&1
